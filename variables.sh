#Description ... in this script we'll be talking about vars in SHELL

#variable_name=variable_value

NAME="scaler var : it can only hold one value at the time"
echo $NAME

#We can use "readonly" "var" to make "var" on mode readonly
#Ex
# readonly NAME
# #now if we try to assign another value to NAME we'll get an error

#We use "unset" to remove var from var list
#Ex
# unset NAME
# #now if we try to display NAME with "echo" we wont have anything on the screen

########## SPECIAL VARS #############
echo "file name: $0"
echo "file parameter: $1"
echo "second parameter: $2"
echo "quoted values: $@"
echo "quoted name: $*"
echo "quoted name: $#"

#Ex of the utility of special vars
# $ ./"file.sh with special vars | variable.sh in this case" "enter a parameter | a text for ex"

#We're not arrived yet to functions but the following statement is just to try
for thing in $*
do
        echo $thing
done

