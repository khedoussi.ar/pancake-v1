############ SHELL LOOPS ############

# while , for, until, nested loops and  loop control

# ---------------- FOR -------------- #
# for item in iter
# do
#       "what you wanna do"
# done

# --------------- WHILE -----------
# while command
# do
#       statement
# done
#Ex
a=0  #assign

while [ $a -lt 10 ]
do
        echo $a
        a=`expr $a + 1`
done

